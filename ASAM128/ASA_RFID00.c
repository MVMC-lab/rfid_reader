#include "ASA_RFID00.h"

char ASA_RFID00_search(char ASAID, char *Card_UID, char *M_status){
	if(ASAID<0||ASAID>8)return 1;
	DDRB=DDRB|(7<<5);
	PORTB=PORTB|(ASAID<<5);
	char S_status=0;
	char check;
	int count=0,count_max=10;
	*M_status = 1;
	check = M128_UARTM_trm(0, 0, 0x87, 0, 1, M_status);
	if(check !=0) return 1;
	while(S_status==0 && count<count_max){
		count = count+1;
		_delay_ms(10);
		check = M128_UARTM_rec(0, 0, 0x87, 1, 1, &S_status);
		if(check !=0) return 1;
	}
	if(count>=count_max) return 2;
		//bad card or no card
	if(S_status==1){
		//get UID
		*M_status=1;
		check = M128_UARTM_rec(0, 0, 0x87, 2, 4, Card_UID);
		if(check !=0) return 1;
		return 0;
	}
	else{
		// unexpect error
		return S_status+100;
	}
}
char ASA_RFID00_read(char ASAID, char targetblock, char *password, char *data, char *M_status){
	if(ASAID<0||ASAID>8) return 1;
	DDRB=DDRB|(7<<5);
	PORTB=PORTB|(ASAID<<5);
	char S_status=0;
	char check;
	int count=0,count_max=200;
	char error_code=0;
	check = M128_UARTM_trm(0,0,0x87,3,1,&targetblock);
	if(check !=0) return 1;
	check = M128_UARTM_trm(0,0,0x87,4,6,password);
	if(check !=0) return 1;
	*M_status = 2;
	check = M128_UARTM_trm(0, 0, 0x87, 0, 1, M_status);
	if(check !=0) return 1;
	check = M128_UARTM_rec(0, 0, 0x87, 1, 1, &S_status);
	if(check !=0) return 1;
	while(S_status==1 && count<count_max){
		count = count+1;
		check = M128_UARTM_rec(0, 0, 0x87, 1, 1, &S_status);
		if(check !=0) return 1;
		check = M128_UARTM_rec(0,0,0x87,6,1,&error_code);
		if(check !=0) return 1;
		if(error_code !=0)return error_code;
	}
	if(count>=count_max){
		//time out
		*M_status = 0;
		return 5;
	}
	if(S_status==3){
		//waiting execution
		*M_status = 5;
		check = M128_UARTM_trm(0, 0, 0x87, 0, 1,M_status);
		if(check !=0) return 1;
		// _delay_ms(100);
		check = M128_UARTM_rec(0, 0, 0x87, 1, 1, &S_status);
		if(check !=0) return 1;
		if(S_status==3){
			//read sucess
			*M_status = 3;
			check = M128_UARTM_rec(0,0,0x87,5,16,data);
			if(check !=0) return 1;
			return 0;
		}
		else{
			check = M128_UARTM_rec(0,0,0x87,6,1,&error_code);
			if(check !=0) return 1;
			return error_code;
		}
	}
	else{
		check = M128_UARTM_rec(0,0,0x87,6,1,&error_code);
		if(check !=0) return 1;
		return error_code;
	}
}
char ASA_RFID00_write(char ASAID, char targetblock, char *password, char *data, char *M_status){
	if(ASAID<0||ASAID>8)return 1;
	DDRB=DDRB|(7<<5);
	PORTB=PORTB|(ASAID<<5);
	char S_status=0;
	char check;
	char error_code;
	int count=0,count_max=200;
	check = M128_UARTM_trm(0,0,0x87,3,1,&targetblock);
	if(check !=0) return 1;
	check = M128_UARTM_trm(0,0,0x87,4,6,password);
	if(check !=0) return 1;
	*M_status = 2;
	check = M128_UARTM_trm(0, 0, 0x87, 0, 1, M_status);
	check =M128_UARTM_rec(0, 0, 0x87, 1, 1, &S_status);
	if(check !=0) return 1;
	while(S_status==1 && count<count_max && error_code==0){
		count = count+1;
		check = M128_UARTM_rec(0, 0, 0x87, 1, 1, &S_status);
		if(check !=0) return 1;
		check = M128_UARTM_rec(0,0,0x87,6,1,&error_code);
		if(check !=0) return 1;
		if(error_code !=0)return error_code;
	}
	if(check !=0) return 1;
	if(count>=count_max){
		//time out
		*M_status = 0;
		return 5;
	}
	if(S_status==3){
		//waiting execution
		check = M128_UARTM_trm(0, 0, 0x87, 5, 16,data);
		if(check !=0) return 1;
		*M_status = 4;
		check = M128_UARTM_trm(0, 0, 0x87, 0, 1,M_status);
		if(check !=0) return 1;
		// _delay_ms(100);
		check = M128_UARTM_rec(0, 0, 0x87, 1, 1, &S_status);
		if(check !=0) return 1;
		if(S_status==3){
			//write sucess
			*M_status = 3;
			return 0;
		}
		else{
			check = M128_UARTM_rec(0,0,0x87,6,1,&error_code);
			if(check !=0) return 1;
			return error_code;
		}
	}
	else{
		check = M128_UARTM_rec(0,0,0x87,6,1,&error_code);
		if(check !=0) return 1;
		return error_code;
	}
}
void ASA_RFID00_logout(char ASAID, char *M_status){
	DDRB=DDRB|(7<<5);
	PORTB=PORTB|(ASAID<<5);
	*M_status = 0;
	M128_UARTM_trm(0, 0, 0x87, 0, 1,M_status);
}
