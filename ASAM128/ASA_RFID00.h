#ifndef ASA_RFID_H
#define ASA_RFID_H

#include "ASA_Lib.h"

char ASA_RFID00_search(char ASAID, char *Card_UID, char *M_status);
char ASA_RFID00_read(char ASAID, char targetblock, char *password, char *data, char *M_status);
char ASA_RFID00_write(char ASAID, char targetblock, char *password, char *data, char *M_status);
void ASA_RFID00_logout(char ASAID, char *M_status);

#endif
