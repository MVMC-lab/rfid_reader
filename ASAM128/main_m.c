#include "ASA_Lib.h"
#include "ASA_RFID00.h"

#define USART_baud 38400
#define BAUDRATE (F_CPU/16/USART_baud-1)

int main(){
	ASA_M128_set();
	char UID[4];
	char key[6]={0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	char targetblock = 0;
	char M_status = 0;
	char S_status = 0;
	char check;
	int a,c=0;

	char data_Wtest[16]={158,158,242,253,15,8,4,0,98,99,100,101,102,103,104,105};
	// 電梯卡(已鎖)
	char data_Wtest2[16]={0xea, 0x52, 0x21, 0x15, 0x8c, 0x08, 0x04, 0x00, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69};
	//	系辦電梯卡
	char data_W[16]={0xea, 0x52, 0x21, 0x15, 0x8c, 0x08, 0x04, 0x00, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69};
	char data_R[16]={0};
	UBRR1H = (unsigned char)(BAUDRATE>>8);
	UBRR1L = (unsigned char)BAUDRATE;
	UCSR1B |= (1<<RXEN1) | (1<<TXEN1)  | (1<<RXCIE1);
	UCSR1C |= (3<<UCSZ10);
	printf("---------strat--------------\n");

	while(1){
		check = ASA_RFID00_search(1,UID, &M_status);
		if(c==0)	printf("\rcard searching   \b\b\b");
		if(check==0){
			printf("\n");
			printf("UID: %x %x %x %x\n",UID[0],UID[1],UID[2],UID[3]);
			printf("enter 1 to save card UID, 2 to over write card UID\n");
			scanf("%d",&a);
			printf("\n");
			if(a==0){
				check = ASA_RFID00_read(1,targetblock,key, data_R,&M_status);
				if(check==0){
					for(int i=0;i<16;i++){
						printf("%x ",*(data_R+i));
					}
					printf("\n" );
				}
				else{
					printf("read fault, EC: %d\n",check );
				}
			}
			else if(a==1){
				data_W[0]=UID[0];
				data_W[1]=UID[1];
				data_W[2]=UID[2];
				data_W[3]=UID[3];
				data_W[4]=UID[0]^UID[1]^UID[2]^UID[3];
				printf("save OK!\n");
				printf("UID saved : %x %x %x %x %x\n",data_W[0],data_W[1],data_W[2],data_W[3],data_W[4]);
			}
			else if(a==2){
				if(data_W[4] == (data_W[0]^data_W[1]^data_W[2]^data_W[3])){
					check = ASA_RFID00_write(1,targetblock,key, data_W,&M_status);
					if(check==0){
						printf("write OK!\n");
					}
					else{
						printf("write fault\n");
					}
				}
				else{
					printf("UID format Error\n");
				}
			}
			else{
			}
			c=0;
			_delay_ms(1000);
			printf("logout\n");
			_delay_ms(3000);
			ASA_RFID00_logout(1, &M_status);
		}
		else{
			printf(".");
			c=(c+1)%3;
			_delay_ms(300);
		}
	}

	while(0){
		// USART TEST
		check = M128_UARTM_rec(0, 0, 0x87, 1, 1, &S_status);
		printf("C %d\n",check );
		printf("S %d\n",S_status );
		check = M128_UARTM_rec(0, 0, 0x87, 6, 1, &S_status);
		printf("C.C. %d\n",check );
		printf("EC %x\n",S_status );
		_delay_ms(500);
	}

}
