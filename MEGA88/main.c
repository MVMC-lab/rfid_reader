#include <avr\io.h>
#include <avr\interrupt.h>
#include <stdio.h>
#include <util\delay.h>
#include "Mega168_SPI.h"
#include "RC522.h"

#define RESET_DDR DDRD
#define RESET_PORT PORTD
#define RESET_PIN PD5

#define LED_UID_DDR     DDRD
#define LED_UID_PORT    PORTD
#define LED_UID      PD4

#define LED_RW_DDR     DDRD
#define LED_RW_PORT    PORTD
#define LED_RW      PD3

#define USART_baud 38400
#define BAUDRATE (F_CPU/16/USART_baud-1)
#define BufferBytes 256
#define UART_ID_RC522 0x87

// unpack stage
#define S_UARTID 	0
#define S_RegAdd 	1
#define S_data 		2

// RegAdd
#define Reg_M_status		0
#define Reg_S_status		1
#define Reg_UID				2
#define Reg_targetblock		3
#define Reg_key				4
#define Reg_data			5
#define Reg_error_code		6

// STATUS
#define S_halt		0
#define S_UIDget	1
#define S_loging	2
#define S_waitRW	3
#define S_writing	4
#define S_reading	5

// ERROR CODE::
#define EC_normal		0
#define EC_timeout		2
#define EC_badcard		3
#define EC_wrongPW		4
#define EC_Npremission	5
#define EC_unexpect		6

typedef struct {
volatile int BWi;
volatile int BRi;
volatile char buffer[BufferBytes];
} TypeOfUARTS_buffer;
TypeOfUARTS_buffer rxBuffer;
TypeOfUARTS_buffer txBuffer;

typedef struct{
	char M_status;
	char S_status;
	//Read only
	char UID[10];
	// Read only
	char targetblock;
	char key[6];
	// data_w and data_r share the same RegAdd 5
	char data_w[16];
	// write only
	char data_r[16];
	// Read only
	char error_code;
	//Read only
}UARTREG;
UARTREG reg;

void txtransmit(void);
void USART0_init(void);
void rxbuffer_init(void);
void txbuffer_init(void);
void UARTReg_init(void);
void IO_init(void);

int main(){
	USART0_init();
	rxbuffer_init();
	txbuffer_init();
	UARTReg_init();
	M168_SPI_Master_init();
	IO_init();
	RESET_PORT &= ~(1<<RESET_PIN);
	_delay_ms(100);
	RESET_PORT |=(1<<RESET_PIN);
	_delay_ms(100);
	// Reset RC522
	RC522_Init();

	char result = 0;
	char unpackstage = S_UARTID;
	char RegAdd = 0;
	int W_index = 0;
	char datatemp = 0;

	while(1){
		//  buffer check and  reg update
		if(rxBuffer.BRi!=rxBuffer.BWi){
			// something in rxbuffer can be read
			switch(unpackstage){
				case S_UARTID:
					//check the UARTID
					if(rxBuffer.buffer[rxBuffer.BRi]==UART_ID_RC522){
						// correct UARTID
						unpackstage = S_RegAdd;
					}
					else{
						// the data is not for this slave
					}
					rxBuffer.BRi=(rxBuffer.BRi+1)%BufferBytes;
				break;
				case S_RegAdd:
					//get the RegAdd
					RegAdd = rxBuffer.buffer[rxBuffer.BRi];
					rxBuffer.BRi = (rxBuffer.BRi+1)%BufferBytes;
					if(RegAdd&0x80){
						//user want to read the Reg
						unpackstage = S_UARTID;
						RegAdd &= 0x7f;
						switch(RegAdd){
							case Reg_M_status:
								txBuffer.buffer[txBuffer.BWi] = reg.M_status;
								txBuffer.BWi=(txBuffer.BWi+1)%BufferBytes;
								txtransmit();
							break;
							case Reg_S_status:
								txBuffer.buffer[txBuffer.BWi] = reg.S_status;
								txBuffer.BWi=(txBuffer.BWi+1)%BufferBytes;
								txtransmit();
							break;
							case Reg_UID:
								for(int i=0;i<4;i++){
									txBuffer.buffer[txBuffer.BWi] = reg.UID[i];
									txBuffer.BWi=(txBuffer.BWi+1)%BufferBytes;
								}
								txtransmit();
							break;
							case Reg_targetblock:
								txBuffer.buffer[txBuffer.BWi] = reg.targetblock;
								txBuffer.BWi=(txBuffer.BWi+1)%BufferBytes;
								txtransmit();
							break;
							case Reg_key:
								for(int i=0;i<MF_KEY_SIZE;i++){
									txBuffer.buffer[txBuffer.BWi] = reg.key[i];
									txBuffer.BWi=(txBuffer.BWi+1)%BufferBytes;
								}
								txtransmit();
							break;
							case Reg_data:
								for(int i=0;i<16;i++){
									txBuffer.buffer[txBuffer.BWi] = reg.data_r[i];
									txBuffer.BWi=(txBuffer.BWi+1)%BufferBytes;
								}
								txtransmit();
							break;
							case Reg_error_code:
								txBuffer.buffer[txBuffer.BWi] = reg.error_code;
								txBuffer.BWi=(txBuffer.BWi+1)%BufferBytes;
								txtransmit();
							break;
							default :
								// this RegAdd do not exist,
								// so the UARTID maybe other's data
								unpackstage = S_UARTID;
							break;
						}
					}
					else{
						//user want to write the Reg
						switch(RegAdd){
							case Reg_M_status:
							case Reg_S_status:
							case Reg_UID:
							case Reg_targetblock:
							case Reg_key:
							case Reg_data:
							case Reg_error_code:
								unpackstage = S_data;
							break;
							default :
								// this RegAdd do not exist,
								// so the UARTID maybe other's data
								unpackstage = S_UARTID;
							break;
						}
					}
				break;
				case S_data:
					//get the data, and write it in Reg
					datatemp = rxBuffer.buffer[rxBuffer.BRi];
					rxBuffer.BRi=(rxBuffer.BRi+1)%BufferBytes;
					switch(RegAdd){
						case Reg_M_status:
							reg.M_status = datatemp;
							unpackstage = S_UARTID;
						break;
						case Reg_S_status:
							// U should not write this register
							unpackstage = S_UARTID;
						break;
						case Reg_UID:
							// U should not write this register
							W_index = W_index+1;
							if(W_index==4){
								// UID has 4 Bytes
								W_index = 0;
								unpackstage = S_UARTID;
							}
						break;
						case Reg_targetblock:
							reg.targetblock = datatemp;
							unpackstage = S_UARTID;
						break;
						case Reg_key:
							reg.key[W_index] = datatemp;
							W_index = W_index+1;
							if(W_index==6){
								//keyA has 6 Bytes
								W_index = 0;
								unpackstage = S_UARTID;
							}
						break;
						case Reg_data:
							reg.data_w[W_index] = datatemp;
							W_index = W_index+1;
							if(W_index==16){
								//data in a block has 16 Bytes
								W_index = 0;
								unpackstage = S_UARTID;
							}
						break;
						case Reg_error_code:
							// U should not write this register
							unpackstage = S_UARTID;
						break;
					}
				break;
			}
		}
		// buffer check and  reg update

		// execute MFRC522
		else{
			switch(reg.S_status){
				case S_halt:
					result = RC522_NewCardPresent();
					// find card
					if(result==1){
						// new card  has been found
						result = RC522_ReadCardSerial();
						// try to get card UID
						if(result==1){
							// get UID successfully
							for(int i = 0;i<4;i++){
								reg.UID[i] = uid.uidByte[i];
							}
							reg.S_status = S_UIDget;
							reg.M_status = S_UIDget;
							// when Slave got card UID, it will maintain in UIDget Status
							LED_UID_PORT |= (1<<LED_UID);
						}
						else{
							reg.error_code = EC_badcard;
							for(int i = 0;i<4;i++){
								reg.UID[i] = 0;
							}
						}
					}
					else {
						for(int i = 0;i<4;i++){
							reg.UID[i] = 0;
						}
					}
					break;
				case S_UIDget:
					if(reg.M_status==S_halt){
						RC522_HaltA();
						RC522_StopCrypto1();
						reg.S_status = S_halt;
						LED_UID_PORT &= ~(1<<LED_UID);
					}
					else if(reg.M_status==S_loging){
						reg.error_code = EC_normal;
						char blockAddr = (reg.targetblock/4)*4+3;
						result = RC522_Authenticate(PICC_CMD_MF_AUTH_KEY_A, blockAddr, reg.key, &uid);
						if(result==STATUS_OK){
							reg.S_status = S_waitRW;
							LED_UID_PORT &= ~(1<<LED_UID);
							LED_RW_PORT |= (1<<LED_RW);
						}
						else {
							RC522_HaltA();
							RC522_StopCrypto1();
							reg.S_status = S_halt;
							reg.error_code = EC_wrongPW;
							LED_UID_PORT &= ~(1<<LED_UID);
						}
					}
					break;
				case S_waitRW:
					if(reg.M_status==S_halt){
						RC522_HaltA();
						RC522_StopCrypto1();
						reg.S_status = S_halt;
						LED_RW_PORT &= ~(1<<LED_RW);
					}
					else if(reg.M_status==S_writing){
						result = RC522_MIFARE_Write(reg.targetblock, reg.data_w, 16);
						if(result==STATUS_OK){
							reg.M_status=S_waitRW;
						}
						else if(result==STATUS_TIMEOUT){
							RC522_HaltA();
							RC522_StopCrypto1();
							reg.S_status = S_halt;
							reg.error_code = EC_timeout;
							LED_RW_PORT &= ~(1<<LED_RW);
						}
						else{
							RC522_HaltA();
							RC522_StopCrypto1();
							reg.S_status = S_halt;
							reg.error_code = EC_Npremission;
							LED_RW_PORT &= ~(1<<LED_RW);
						}
					}
					else if(reg.M_status==S_reading){
						char buffer_temp[18];
						result = RC522_MIFARE_Read(reg.targetblock, buffer_temp, 18);
						if(result==STATUS_OK){
							for(int i=0;i<16;i++){
								reg.data_r[i] = buffer_temp[i];
							}
							reg.M_status=S_waitRW;
						}
						else{
							RC522_HaltA();
							RC522_StopCrypto1();
							reg.S_status = S_halt;
							reg.error_code = EC_timeout;
							LED_RW_PORT &= ~(1<<LED_RW);
						}
					}
					break;
			}
		}
		// end of execute MFRC522
	}
	return 0;
}

ISR(USART_RX_vect){
	if(((rxBuffer.BWi+1)%BufferBytes)!=rxBuffer.BRi){
		rxBuffer.buffer[(int)rxBuffer.BWi] = UDR0;
		rxBuffer.BWi=(rxBuffer.BWi+1)%BufferBytes;
	}
}
ISR(USART_TX_vect){
	txtransmit();
}

void txtransmit(void){
	if(txBuffer.BRi!=txBuffer.BWi){
		UDR0 = txBuffer.buffer[(int)txBuffer.BRi];
		txBuffer.BRi=(txBuffer.BRi+1)%BufferBytes;
	}
}

void USART0_init(void){
	UBRR0H = (unsigned char)(BAUDRATE>>8);
	UBRR0L = (unsigned char)BAUDRATE;
	UCSR0B |= (1<<RXEN0) | (1<<TXEN0)  | (1<<RXCIE0) | (1<<TXCIE0);
	UCSR0C |= (3<<UCSZ00);
	sei();
}

void rxbuffer_init(void){
	rxBuffer.BWi = 0;
	rxBuffer.BRi = 0;
	for (int i = 0; i < BufferBytes; i++) {
		rxBuffer.buffer[i] = 0;
	}
}

void txbuffer_init(void){
	txBuffer.BWi = 0;
	txBuffer.BRi = 0;
	for (int i = 0; i < BufferBytes; i++) {
		txBuffer.buffer[i] = 0;
	}
}

void UARTReg_init(void){
	reg.M_status = S_UIDget;
	// when Slave got card UID, it will maintain in UIDget Status

	// reg.M_status = S_halt;
	// when Slave got card UID, it will try to go back S_halt Status

	reg.S_status = S_halt;
	for(int i = 0;i<10;i++){
		reg.UID[i] = 0;
	}
	reg.targetblock = 0;
	for(int i = 0;i<6;i++){
		reg.key[i] = 0;
	}
	for(int i = 0;i<16;i++){
		reg.data_w[i] = 0;
	}
	for(int i = 0;i<16;i++){
		reg.data_r[i] = 0;
	}
	reg.error_code = EC_normal;
}

void IO_init(void){
	LED_RW_DDR |= (1<<LED_RW);
	LED_UID_DDR |= (1<<LED_UID);
	RESET_DDR |= (1<<RESET_PIN);
	LED_RW_PORT &= ~(1<<LED_RW);
	LED_UID_PORT &= ~(1<<LED_UID);
}
