#ifndef MEGA168_SPI_h
#define MEGA168_SPI_h
#include <avr/io.h>
#include <util\delay.h>

#define SPI_DDR     DDRB
#define SPI_PORT    PORTB
#define SPI_SS      PB2
#define SPI_SCK     PB5
#define SPI_MOSI    PB3
#define SPI_MISO    PB4

#define SPI_CS_DDR     DDRD
#define SPI_CS_PORT    PORTD
#define SPI_CS      PD6


void M168_SPI_Master_init(void);
char SPI_swap(char Data);
void SPI_CSL(void);
void SPI_CSH(void);

#endif
