#include "Mega168_SPI.h"

void M168_SPI_Master_init(void){
	SPI_DDR&=~((1<<SPI_SS)|(1<<SPI_SCK)|(1<<SPI_MOSI)|(1<<SPI_MISO));
	SPI_DDR|=(1<<SPI_SS)|(1<<SPI_SCK)|(1<<SPI_MOSI);
	SPI_CS_DDR|=(1<<SPI_CS);
	SPCR=(0<<SPIE)|(1<<SPE)|(0<<DORD)|(1<<MSTR)|(0<<CPOL)|(0<<CPHA)|(0<<SPR1)|(0<<SPR0);
	SPI_CSH();
}
char SPI_swap(char Data){
	SPDR = Data;
	while( !(SPSR&(1<<SPIF)) );
	return SPDR;
}

void SPI_CSL(void){
	SPI_CS_PORT &= ~(1<<SPI_CS);
	// _delay_us(1);
}

void SPI_CSH(void){
	SPI_CS_PORT |= (1<<SPI_CS);
	_delay_us(30);
}
